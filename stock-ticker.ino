// This #include statement was automatically added by the Particle IDE.
#include <Adafruit_ILI9341.h>
#include <Adafruit_mfGFX.h>
#include <WebServer.h>
#include <ArduinoJson.h>
// sources
// https://www.hackster.io/wgbartley/iot-device-management-with-mdns-and-webduino-93982a?ref=user&ref_id=988&offset=0
// arduino json assistant
// alphavantage

//webduino
#define WEBDUINO_FAVICON_DATA ""
#define WEBDUINO_FAIL_MESSAGE ""
#define POST_NAME_LENGTH    32
#define POST_VALUE_LENGTH   32
WebServer webserver("", 80);

//display
#define HEIGHT  240
#define WIDTH 320
Adafruit_ILI9341 tft = Adafruit_ILI9341(A2, A1, A0); //(uint8_t cs, uint8_t dc, uint8_t rst)

//Alphavantage api key
#define API_KEY ""

const int maxNumOfStocks = 20;
const char* project = "IOT Stock Ticker";
const char* intro = "a custom project by";
const char* author = " Aaron Peterson";
const char* message = "for Doug. Love you dad!";
const size_t bufferSize = JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(10) + 260;

int addr = 10;
int stockArrayIndex = 0;
int eggTimer = 0;
int numOfStocks = 0;
int updateInterval = 60000; //ms
char stockList[100];
bool updateScreen = true;
char stockArray[maxNumOfStocks][6];
char* json;

DynamicJsonBuffer jsonBuffer(bufferSize);

// All pages
P(Page_start) = "<!DOCTYPE html><html><head><title>My Stock Ticker</title><meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"><meta charset=\"UTF-8\"></head><body>";
P(Page_css) = "<style type=\"text/css\">html,body{font-family:sans-serif;}fieldset{margin-left:auto;margin-right:auto;max-width:480px;border-radius:8px;}</style>";
P(Page_end) = "</body></html>";

// Form-specific
P(Form_css) = "<style type=\"text/css\">p{clear:both;width:100%;line-height:1.5em;}label{width:49%;text-align:right;display:inline-block;line-height:1.5em;float:left;margin-left:-1em;}select,input[type=\"text\"]{width:49%;text-align:left;float:right;}</style>";
P(Form_settings) = "<form method=\"POST\" action=\"save.html\"><fieldset><legend>Stock Ticker Config</legend><p><label for=\"b\">Stock List</label><input type=\"text\" id=\"b\" name=\"b\"></p><p> </p><p><label for=\"s\"></label><input type=\"submit\" id=\"s\" value=\"Save\"></p></fieldset>";
P(Form_javascript1) = "<script type=\"text/javascript\">";
P(Form_javascript2) = "window.onload=function(){$b=document.querySelector('#b');$b.value=b;}";
P(Form_javascript3) = "</script>";

// Save page
P(Save_fieldset) = "<fieldset><legend>Stock Ticker Config</legend><p>Your settings have been saved. You will be redirected in 5 seconds, or click <a href=\"/\">here</a> to continue.</p></fieldset>";
P(Save_redirect) = "<meta http-equiv=\"refresh\" content=\"5;URL=/\">";

// Fail page
P(Fail_message) = "<p>error</p>";

// index.html
void web_index(WebServer &server, WebServer::ConnectionType type, char *, bool)
{
    server.httpSuccess();
    server.printP(Page_start);
    server.printP(Form_settings);
    server.printP(Page_css);
    server.printP(Form_css);
    server.printP(Form_javascript1);
    server.printP("var b=\""+String(stockList)+"\";");
    server.printP(Form_javascript2);
    server.printP(Form_javascript3);
    server.printP(Page_end);
}

// save.html
void web_save(WebServer &server, WebServer::ConnectionType type, char *, bool)
{
    URLPARAM_RESULT rc;
    char name[POST_NAME_LENGTH];
    char value[POST_VALUE_LENGTH];

    server.httpSuccess();

    server.printP(Page_start);
    server.printP(Save_fieldset);
    server.printP(Save_redirect);
    server.printP(Page_css);
    server.printP(Page_end);

    // Loop through POSTed data
    while(server.readPOSTparam(name, POST_NAME_LENGTH, value, POST_VALUE_LENGTH))
    {
        // Because strings are easier to test/manipulate
        String _name = String(name);
        String _value = String(value);

        if(_name.equals("b"))
        {
            strcpy(stockList, _value);
            EEPROM.put(addr, stockList);
            buildStockArray();
        }
    }
}

// Bad requests
void web_fail(WebServer &server, WebServer::ConnectionType type, char *, bool) 
{
    server.httpFail();
    server.printP(Page_start);
    server.printP(Fail_message);
    server.printP(Page_end);
}

void parseResults(const char *event, const char *data) 
{
    json = strdup(data);
    updateScreen = true;
}

void drawStringCenter(int16_t x, int16_t y, const char *string, int8_t size, uint16_t fgColor)
{
    if(string!=NULL)
    {
        tft.setTextColor(fgColor);
	    tft.setTextSize(size);
	    tft.setCursor(x - (strlen(string)*3*size), y-(4*size));
    	while(*string)
	        tft.write(*string++);
    }
}

char* round_char(const char* char_str, int num)
{
    char* s = strdup(char_str);
    int len = strlen(s);
    s[len-num] = '\0';
    return s;
}

void buildStockArray()
{
    EEPROM.get(addr, stockList);
    
    numOfStocks = 0;
    stockArrayIndex = 0;
    char stockChar[60];
    strcpy(stockChar, stockList);
    char *ptr = strtok(stockChar, ",");
    
    while(ptr)
    {
        strcpy(stockArray[numOfStocks], ptr);
        ptr = strtok(NULL, ",");
        numOfStocks++;
    }
}

void setup()
{
    
    Serial.println("Adafruit 2.2\" SPI TFT Test!");
    tft.begin();
    tft.setRotation(3);
    tft.fillScreen(ILI9341_BLACK);
    
    drawStringCenter(WIDTH/2, 30, project, 3, ILI9341_WHITE );
    drawStringCenter(WIDTH/2, (HEIGHT/2)-30, intro, 2, ILI9341_WHITE );
    drawStringCenter(WIDTH/2, (HEIGHT/2), author, 2, ILI9341_WHITE );
    drawStringCenter(WIDTH/2, HEIGHT-20, message, 2, ILI9341_WHITE );
    
    Particle.subscribe("hook-response/get_stock_price", parseResults, MY_DEVICES);
	
    webserver.setDefaultCommand(&web_index);
    webserver.setFailureCommand(&web_fail);
    webserver.addCommand("index.html", &web_index);
    webserver.addCommand("save.html", &web_save);
    webserver.begin();
    
    buildStockArray();
    
    delay(10000);
}

void loop()
{
    char web_buff[64];
    int web_len = 64;
    webserver.processConnection(web_buff, &web_len);
    
    //non blocking loop
    if(millis() > eggTimer)
    {
        if(numOfStocks > 0)
        {
            if(stockArrayIndex >= numOfStocks)
            {
                stockArrayIndex = 0;
            }
            
            if(stockArray[stockArrayIndex])
            {
                Particle.publish("get_stock_price",stockArray[stockArrayIndex], PRIVATE);
            }
            
            eggTimer = millis() + updateInterval;
            
            stockArrayIndex++;
        }
    }
    
    if(updateScreen)
    {
        JsonObject& root = jsonBuffer.parseObject(json);
        JsonObject& Global_Quote = root["Global Quote"];
        const char* Global_Quote_01_symbol = Global_Quote["01. symbol"]; // "MSFT"
        const char* Global_Quote_05_price = Global_Quote["05. price"]; // "102.1475"
        const char* Global_Quote_10_change_percent = Global_Quote["10. change percent"]; // "-0.6280%"
        
        String tmp = WiFi.localIP();
        const char * ipAddress = tmp.c_str();
        
        tft.fillScreen(ILI9341_BLACK);
        yield();
        
        drawStringCenter(WIDTH/2, 40, Global_Quote_01_symbol, 4, ILI9341_WHITE );
        
        if(Global_Quote_10_change_percent[0] == '-')
        {
            drawStringCenter(WIDTH/2, (HEIGHT/2)-10, round_char(Global_Quote_05_price, 2), 5, ILI9341_RED );
            drawStringCenter(WIDTH/2, (HEIGHT/2)+55, Global_Quote_10_change_percent, 2, ILI9341_RED );
        }
        else
        {
            drawStringCenter(WIDTH/2, (HEIGHT/2)-10, round_char(Global_Quote_05_price, 2), 5, ILI9341_GREEN );
            drawStringCenter(WIDTH/2, (HEIGHT/2)+55, Global_Quote_10_change_percent, 2, ILI9341_GREEN );
        }
        
        drawStringCenter(WIDTH/2, HEIGHT-15, ipAddress , 1, ILI9341_WHITE );
        
        updateScreen = false;
    }
}